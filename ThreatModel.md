# Threatspec Project Threat Model

A threatspec project.


# Diagram
![Threat Model Diagram](ThreatModel.md.png)



# Exposures

## Xss injection against WebApp:App
Insufficient input validation

```
// @exposes WebApp:App to XSS injection with insufficient input validation
func editHandler(w http.ResponseWriter, r *http.Request, title string) {
	p, err := loadPage(title)
	if err != nil {
		p = &Page{Title: title}
	}

```
/home/archizgod/threatspec_examples/simple_web.go:1

## Content injection against WebApp:App
Insufficient input validation

```
// @exposes WebApp:App to content injection with insufficient input validation
func saveHandler(w http.ResponseWriter, r *http.Request, title string) {
	body := r.FormValue("body")
	p := &Page{Title: title, Body: []byte(body)}
	err := p.save()
	if err != nil {

```
/home/archizgod/threatspec_examples/simple_web.go:1

## Sql injection against MyApp:Web:AppServer
Possible lack of input validation and sanitization

```

```
/home/archizgod/threatspec_examples/myapp_product.threatspec.txt:56

## Man in the middle against MyApp:Web:DataCache
Lack of encryption

```

```
/home/archizgod/threatspec_examples/myapp_product.threatspec.txt:57

## Man in the middle against MyApp:Web:Database
Lack of encryption

```

```
/home/archizgod/threatspec_examples/myapp_product.threatspec.txt:58

## Man in the middle against Petstore:Web
Lack of tls

```
{
  "url": "http://petstore.swagger.io/v1",
  "x-threatspec": "@exposes Petstore:Web to Man in the Middle (#mitm) with lack of TLS"
}
```
/home/archizgod/threatspec_examples/petstore.yaml:0

## Creation of fake pets against Petstore:Pet:Create
Lack of authentication
### Description
Any anonymous user can create a pet because there is no authentication and authorization


```
{
  "summary": "Create a pet",
  "operationId": "createPets",
  "tags": [
    "pets"
  ],
  "x-threatspec": {
    "@exposes Petstore:Pet:Create to Creation of fake pets with lack of authentication": {
      "description": "Any anonymous user can create a pet because there is no authentication and authorization",
      "action": "expose",
      "component": "Petstore:Pet:Create",
      "threat": "Creation of fake pets",
      "details": "lack of authentication"
    }
  },
  "responses": {
    "201": {
      "description": "Null response"
    },
    "default": {
      "description": "unexpected error",
      "content": {
        "application/json": {
          "schema": {
            "$ref": "#/components/schemas/Error"
          }
        }
      }
    }
  }
}
```
/home/archizgod/threatspec_examples/petstore.yaml:0


# Acceptances

## Arbitrary file writes to WebApp:FileSystem
Filename restrictions

```
// @accepts arbitrary file writes to WebApp:FileSystem with filename restrictions
func (p *Page) save() error {
	filename := p.Title + ".txt"
	return ioutil.WriteFile(filename, p.Body, 0600)
}


```
/home/archizgod/threatspec_examples/simple_web.go:1

## Arbitrary file reads to WebApp:FileSystem
Filename restrictions

```
// @accepts arbitrary file reads to WebApp:FileSystem with filename restrictions
func loadPage(title string) (*Page, error) {
	filename := title + ".txt"
	body, err := ioutil.ReadFile(filename)
	if err != nil {
		return nil, err

```
/home/archizgod/threatspec_examples/simple_web.go:1


# Transfers


# Mitigations

## Unauthorised access against WebApp:FileSystem mitigated by Strict file permissions


```
// @mitigates WebApp:FileSystem against unauthorised access with strict file permissions
func (p *Page) save() error {
	filename := p.Title + ".txt"
	return ioutil.WriteFile(filename, p.Body, 0600)
}


```
/home/archizgod/threatspec_examples/simple_web.go:1

## Resource access abuse against WebApp:Web mitigated by Basic input validation


```
// @mitigates WebApp:Web against resource access abuse with basic input validation
func makeHandler(fn func(http.ResponseWriter, *http.Request, string)) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		m := validPath.FindStringSubmatch(r.URL.Path)
		if m == nil {
			http.NotFound(w, r)

```
/home/archizgod/threatspec_examples/simple_web.go:1

## Privilege escalation against WebApp:Web mitigated by Non-privileged port


```
// @mitigates WebApp:Web against privilege escalation with non-privileged port
func main() {
	flag.Parse()
	http.HandleFunc("/view/", makeHandler(viewHandler))
	http.HandleFunc("/edit/", makeHandler(editHandler))
	http.HandleFunc("/save/", makeHandler(saveHandler))

```
/home/archizgod/threatspec_examples/simple_web.go:1
### Tests

#### Non-privileged port for WebApp:Web

```
// @tests non-privileged port for WebApp:Web

```
/home/archizgod/threatspec_examples/simple_web.go:1




# Reviews

## WebApp:Web
Is this a security feature?

```
		err = ioutil.WriteFile("final-port.txt", []byte(l.Addr().String()), 0644) // @review WebApp:Web Is this a security feature?
		if err != nil {
			log.Fatal(err)
		}
		s := &http.Server{}
		s.Serve(l)

```
/home/archizgod/threatspec_examples/simple_web.go:1


# Connections

## User:Browser To WebApp:Web
HTTP:8080

```
	http.ListenAndServe(":8080", nil) // @connects User:Browser to WebApp:Web with HTTP:8080

}


```
/home/archizgod/threatspec_examples/simple_web.go:1

## External:User To MyApp:Web:LoadBalancer
HTTPS on 443

```

```
/home/archizgod/threatspec_examples/myapp_product.threatspec.txt:25

## MyApp:Web:LoadBalancer To MyApp:Web:WebServer
HTTPS on 8443

```

```
/home/archizgod/threatspec_examples/myapp_product.threatspec.txt:26

## MyApp:Web:WebServer To MyApp:Web:AppServer
HTTPS on 8443

```

```
/home/archizgod/threatspec_examples/myapp_product.threatspec.txt:27

## MyApp:Web:AppServer To MyApp:Web:DataCache
TCP/6379

```

```
/home/archizgod/threatspec_examples/myapp_product.threatspec.txt:28

## MyApp:Web:AppServer To MyApp:Web:Database
TCP/3306

```

```
/home/archizgod/threatspec_examples/myapp_product.threatspec.txt:29

## External:User To MyApp:Auth:Login
submits credentials

```

```
/home/archizgod/threatspec_examples/myapp_product.threatspec.txt:32

## MyApp:Auth:Login To External:User
auth session token on success

```

```
/home/archizgod/threatspec_examples/myapp_product.threatspec.txt:33

## External:User To MyApp:Product:Search
search by keyword

```

```
/home/archizgod/threatspec_examples/myapp_product.threatspec.txt:35

## MyApp:Product:Search To External:User
list of matching products

```

```
/home/archizgod/threatspec_examples/myapp_product.threatspec.txt:36

## External:User To MyApp:Product:Basket
adds desired products to basket

```

```
/home/archizgod/threatspec_examples/myapp_product.threatspec.txt:38

## External:User To MyApp:Product:Payment
payment of products in basket

```

```
/home/archizgod/threatspec_examples/myapp_product.threatspec.txt:39

## MyApp:Product:Payment To External:User
payment summary shown on success

```

```
/home/archizgod/threatspec_examples/myapp_product.threatspec.txt:40


# Components

## WebApp:Web

## WebApp:FileSystem

## WebApp:App

## MyApp:Web:AppServer

## MyApp:Web:DataCache

## MyApp:Web:Database

## Petstore:Web

## Petstore:Pet:Create

## User:Browser

## MyApp:Auth:Login

## External:User

## MyApp:Web:LoadBalancer

## MyApp:Web:WebServer

## MyApp:Product:Search

## MyApp:Product:Basket

## MyApp:Product:Payment


# Threats

## Unauthorised access


## Resource access abuse


## Privilege escalation


## Xss injection


## Content injection


## Sql injection

### Description
An attacker can inject malicious SQL statements, typically through web form components, and make changes or retrieve information from the backend database.


## Man in the middle

### Description
An attacker can intercept unsecured traffic and can obtain sensitive information or tamper with the connection.


## Creation of fake pets


## @cwe_319_cleartext_transmission


## Authentication token theft

### Description
An attacker can steal or otherwise obtain a sensitive authentication token and can therefore spoof the legitimate user


## Arbitrary file writes


## Arbitrary file reads



# Controls

## Non-privileged port

## Strict file permissions

## Basic input validation
